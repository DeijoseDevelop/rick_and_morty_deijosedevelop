import {useState, useEffect, Fragment} from 'react';
import Character from './Character';
import axios from 'axios';
import Pagination from 'react-js-pagination';

export default function CharacterList({searching}) {
  const [page, setPage] = useState(1);
  const [characters, setCharacters] = useState(null);
  let filteredCharacter = characters?.results;

  if (searching && characters)
    filteredCharacter = characters.results.filter((character) =>
      character.name.toLowerCase().includes(searching.toLowerCase())
    );
  const URL = 'https://rickandmortyapi.com/api/character';
  const LIMIT = 20;
  useEffect(() => {
    axios
      .get(`${URL}?page=${page}`)
      .then((response) => setCharacters(response.data));
  }, [page]);

  function handlePageChange(pageNumber) {
    setPage(pageNumber);
  }
  return (
    <div className='py-5'>
      <div className='d-flex justify-content-center'>
        <Pagination
          itemClass='page-item'
          linkClass='page-link'
          activePage={page}
          itemsCountPerPage={LIMIT}
          totalItemsCount={826}
          onChange={handlePageChange}
        />
      </div>
      <div className='lista row py-1'>
        {filteredCharacter ? (
          filteredCharacter.map((character) => {
            return <Character key={character.id} {...character} />;
          })
        ) : (
          <h3>Cargando...</h3>
        )}
      </div>
      <div className='d-flex justify-content-center'>
        <Pagination
          itemClass='page-item'
          linkClass='page-link'
          activePage={page}
          itemsCountPerPage={LIMIT}
          totalItemsCount={826}
          onChange={handlePageChange}
        />
      </div>
    </div>
  );
}
