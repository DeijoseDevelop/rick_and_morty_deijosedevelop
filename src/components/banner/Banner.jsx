import imgBanner from '../../assets/rick_and_morty.jpg';

export default function Banner() {
  return (
    <div>
      <img
        src={imgBanner}
        className='card-img'
        alt='Banner'
        style={{
          height: '740px',
          width: '100%',
          objectFit: 'contain',
        }}
      />
    </div>
  );
}
