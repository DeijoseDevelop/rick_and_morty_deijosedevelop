import {Link} from 'react-router-dom';

export default function Header() {
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
      <div className='container-fluid'>
        <Link
          className='navbar-brand'
          to='/'
          style={{fontFamily: '"Shadows Into Light", cursive', fontWeight: 'bold'}}
        >
          Rick and Morty
        </Link>
        <div className='collapse navbar-collapse' id='navbar-text'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link
                className='nav-link-active'
                aria-current='page'
                to='/'
              ></Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
