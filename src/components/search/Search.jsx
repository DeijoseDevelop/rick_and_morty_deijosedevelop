export default function Search({valor, onSearch}) {
  return (
    <div className='d-flex justify-content-end'>
      <div className='mb-3 col-5'>
        <input
          type='text'
          className='form-control'
          placeholder='Search character'
          value={valor}
          onChange={(e) => onSearch(e.target.value.trim())}
        />
      </div>
    </div>
  );
}
