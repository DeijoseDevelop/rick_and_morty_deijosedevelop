export default function Footer() {
  return (
    <div className='navbar-dark bg-dark fixed-bottom'>
      <p className='text-white text-center p-3 mb-0'>
        Proyecto creado por Deiver Vazquez CAR IV LSV-Tech
      </p>
    </div>
  );
}
