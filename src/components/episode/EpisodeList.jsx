import {Episode} from './Episode';

export default function EpisodeList({datos}) {
  return (
    <div className='row'>
      {datos.map((episode) => {
        return <Episode {...episode} key={episode.id} />;
      })}
    </div>
  );
}
