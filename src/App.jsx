import Home from './pages/Home';
import {Routes, Route} from 'react-router-dom';
import Header from './components/banner/Header';
import Footer from './components/footer/Footer';
import Episodes from './pages/Episodes';

function App() {
  return (
    <div className="bg-black">
      <Header />
      <Routes>
        <Route path='/' element={<Home />}></Route>
        <Route path='/personaje/:idCharacter' element={<Episodes/>}></Route>
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
