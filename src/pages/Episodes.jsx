import {useState, useEffect} from 'react';
import EpisodeList from '../components/episode/EpisodeList';
import axios from 'axios';
import {useParams} from 'react-router-dom';
import BannerCharacterDetail from '../components/characters/BannerCharacterDetail';

export default function Episodes() {
  let {idCharacter} = useParams();
  const [character, setCharacter] = useState(null);
  const [episodes, setEpisodes] = useState(null);
  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${idCharacter}`)
      .then((response) => {
        setCharacter(response.data);
        let {episode} = response.data;
        let peticiones = episode.map(UrlEpisode => axios.get(UrlEpisode))
        Promise.all(peticiones).then(response => {
            let datosFormat = response.map(episode => episode.data)
            setEpisodes(datosFormat)
        })
      });
  }, []);

  return (
    <div className='p-5'>
      {character ? (
        <div>
          <BannerCharacterDetail {...character} />
          <h2 className='py-4 text-white'>Episodes</h2>
          {episodes ? (
            <EpisodeList datos={episodes} />
          ) : (
            <h4>Loading Episodes</h4>
          )}
        </div>
      ) : (
        <h2>Loading...</h2>
      )}
    </div>
  );
}
