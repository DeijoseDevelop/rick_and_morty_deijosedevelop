import Banner from '../components/banner/Banner';
import CharacterList from '../components/characters/CharacterList';
import Search from '../components/search/Search';
import {useState} from 'react';

export default function Home() {
  const [search, setSearch] = useState('');
  return (
    <div>
      <Banner />
      <div className='px-5 pb-5'>
        <h1 className='py-4 text-white'>Personajes destacados</h1>
        <Search valor={search} onSearch={setSearch} />
        <CharacterList searching={search} />
      </div>
    </div>
  );
}
